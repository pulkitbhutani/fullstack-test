const db = require("../database");

function validateUserRegisterDetails(req, res, next) {
    console.log(req.body);
    if (!req.body.firstName) {
      return res.status(422).json({ errors: { firstName: "can't be blank" } });
    }
  
    if (!req.body.email) {
      return res.status(422).json({ errors: { email: "can't be blank" } });
    }
  
    if (!req.body.password) {
      return res.status(422).json({ errors: { password: "can't be blank" } });
    }
  
    next();
  }
  
  function validateUserLoginDetails(req, res, next) {
    if (!req.body.email) {
      return res.status(422).json({ errors: { email: "can't be blank" } });
    }
  
    if (!req.body.password) {
      return res.status(422).json({ errors: { password: "can't be blank" } });
    }
  
    next();
  }
  
  async function validateIfUserExists(req, res, next) {
    const { email } = req.body;
    const user = await db
      .promise()
      .query(`SELECT * FROM USERS WHERE email ='${email}'`);
    console.log(user);
    if (user[0].length === 0) {
      next();
    } else {
      res.status(403).send({ msg: "User already Exists." });
    }
  }
  
  async function validateIfUserDoesNotExist(req, res, next) {
    const { email } = req.body;
    const user = await db
      .promise()
      .query(`SELECT * FROM USERS WHERE email ='${email}'`);
    console.log(user);
    if (user[0].length >= 1) {
      next();
    } else {
      res.status(403).send({ name: "NoUserFound", message: "User does not Exists." });
    }
  }

  module.exports = {
    validateUserRegisterDetails: validateUserRegisterDetails,
    validateUserLoginDetails : validateUserLoginDetails,
    validateIfUserExists : validateIfUserExists,
    validateIfUserDoesNotExist: validateIfUserDoesNotExist
  };
  