var express = require("express");
var router = express.Router();
const orgController = require("../controllers/OrgController");

//Get Specific Organization
router.get("/:id", orgController.getOrganization);

//Update Organization
router.put("/:id", orgController.updateOrganization);

//Get All Organizations
router.get("/", orgController.getAllOrganizations);

//Add Organization Route
router.post("/add-organization", orgController.addOrganization);

module.exports = router;
