var express = require("express");
const db = require("../database");
const authController = require("../controllers/AuthController");
const checkAuthMiddleware = require("../middleware/check-auth");
const inputValidationMiddleware = require("../middleware/inputValidation");
const utilityFunctions = require("../utility/utility");
var router = express.Router();

/* GET users listing. */
router.get("/", checkAuthMiddleware.checkAuth, async function (req, res) {
  const results = await db.promise().query(`SELECT * FROM USERS`);
  res.status(200).send(results[0]);
});

//Login Post
router.post(
  "/login",
  [
    inputValidationMiddleware.validateUserLoginDetails,
    inputValidationMiddleware.validateIfUserDoesNotExist,
  ],
  authController.login
);

//Register Post
router.post(
  "/register",
  [
    inputValidationMiddleware.validateUserRegisterDetails,
    inputValidationMiddleware.validateIfUserExists,
  ],
  (req, res) => {
    const { firstName, lastName, email, password } = req.body;
    const hashPassword = utilityFunctions.getHashedPassword(password);
    if (email && password) {
      try {
        authController
          .signUp(firstName, lastName, email, hashPassword)
          .then(() => {
            res.status(201).send({ msg: "Created User" });
          });
      } catch (err) {
        console.log(err);
      }
    }
  }
);

module.exports = router;
