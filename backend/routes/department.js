var express = require("express");
var router = express.Router();
const deptController = require("../controllers/DeptController");


//Get All Organization Names
router.get("/organization-names", deptController.getAllOrganizationNames);



module.exports = router;