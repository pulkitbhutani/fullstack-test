const db = require("../database");

function getAllOrganizationNames(req, res) {
    try {
        db.promise()
          .query(`SELECT orgName FROM ORGANIZATIONS`)
          .then((results) => {
            res.status(200).send(results[0]);
          });
      } catch (err) {
        console.log(err);
        res.status(400).send({ message: "Something Went Wrong" });
      }
}

module.exports = {
  getAllOrganizationNames: getAllOrganizationNames,
};
