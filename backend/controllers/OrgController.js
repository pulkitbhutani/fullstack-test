const db = require("../database");

//Get All Organizations
function getAllOrganizations(req, res) {
  try {
    db.promise()
      .query(`SELECT * FROM ORGANIZATIONS`)
      .then((results) => {
        res.status(200).send(results[0]);
      });
  } catch (err) {
    console.log(err);
    res.status(400).send({ message: "Something Went Wrong" });
  }
}

//Get Organization based on ID
function getOrganization(req, res) {
  const id = req.params.id;
  console.log(req);
  console.log(id);
  try {
    db.promise()
      .query(`SELECT * FROM ORGANIZATIONS WHERE id = ${id}`)
      .then((results) => {
        if (results[0].length >= 1) {
          res.status(200).send(results[0][0]);
        } else {
          throw Error("Organization Not Found");
        }
      })
      .catch((err) => {
        console.log(err);
        res
          .status(400)
          .send(
            { message: err.message } || { message: "Something Went Wrong" }
          );
      });
  } catch (err) {
    console.log(err);
    res.status(400).send({ message: "Something Went Wrong" });
  }
}

//Add Orgniation Controller
function addOrganization(req, res) {
  const { orgName, ownerName, address, city, state, country, userId } =
    req.body;

  try {
    return db
      .promise()
      .query(
        `INSERT INTO ORGANIZATIONS (userId, orgName, ownerName, address, city, state, country, createdAt) VALUES 
      ('${userId}', '${orgName}', '${ownerName}', '${address}', '${city}','${state}','${country}', Now())`
      )
      .then(() => {
        res.status(200).send({ message: "New Organization Added!" });
      });
  } catch (err) {
    console.log(err);
    res.status(400).send({ message: "Something Went Wrong" });
  }
}

function updateOrganization(req, res) {
  const { orgName, ownerName, address, city, state, country, id } =
    req.body;
  try {
    return db
      .promise()
      .query(
        `UPDATE ORGANIZATIONS SET orgName = '${orgName}' , ownerName = '${ownerName}', address = '${address}', city = '${city}', state = '${state}', country = '${country}' WHERE id = ${id}`
      )
      .then(() => {
        res.status(200).send({ message: "Organization Updated!" });
      });
  } catch (err) {
    console.log(err);
    res.status(400).send({ message: "Something Went Wrong" });
  }
}

module.exports = {
  addOrganization: addOrganization,
  getAllOrganizations: getAllOrganizations,
  getOrganization: getOrganization,
  updateOrganization: updateOrganization
};
