const db = require("../database");
const jwt = require("jsonwebtoken");
const utilityFunctions = require("../utility/utility");;

//Signup Controller
function signUp(firstName, lastName, email, hashPassword) {
  return db
    .promise()
    .query(
      `INSERT INTO USERS (firstName, lastName, email, password, createdAt) VALUES ('${firstName}', '${lastName}', '${email}', '${hashPassword}', Now())`
    )
    .catch((err) => {
      console.log(err);
    });
}

//Login Controller
async function login(req, res) {
  const { email, password } = req.body;
  const hashedPassword = utilityFunctions.getHashedPassword(password);
  try {
    if (email && password) {
      const user = await db
        .promise()
        .query(`SELECT * FROM USERS WHERE email ='${email}'`);
      const userData = user[0].map((user) => ({
        email: user.email,
        password: user.password,
        id: user.id
      }));

      if (
        userData[0].email === email &&
        userData[0].password === hashedPassword
      ) {
        const token = jwt.sign(
          {
            email: user.email,
            userId: user.indexOf,
          },
          process.env.JWT_KEY,
          function (err, token) {
            if (err) {
              return err;
            }
            res.status(200).json({
              email: userData[0].email,
              id: userData[0].id,
              message: "Authentication Successful!",
              token: token,
            });
          }
        );
      } else {
        res.status(400).send({name: 'InvalidCredentials', message: 'Invalid Credentials!'});
      }
    }
  } catch (err) {
    res.status(400).send({message: err.message});
  }
}

module.exports = {
  signUp: signUp,
  login: login,
};
