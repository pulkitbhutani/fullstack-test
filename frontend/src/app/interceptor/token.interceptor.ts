import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor
} from '@angular/common/http';
import { AccountService } from '../services/account.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  token: string;

  constructor(private accountService: AccountService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    this.token = this.accountService.getToken();

    const authReq = req.clone({
      headers: req.headers.set('Authorization', `Bearer ${this.token}`)
    });

    return next.handle(authReq);
  }

}
