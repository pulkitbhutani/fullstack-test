import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { GlobalConstants } from '../utility/constants';


@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private loggedIn = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {

  }

  getToken() {
    return localStorage.getItem('ACCESS_TOKEN');
  }

  getUserId() {
    const userData = JSON.parse(localStorage.getItem('USER_DETAIL'));
    return userData.id;
  }

  getUserEmail() {
    const userData = JSON.parse(localStorage.getItem('USER_DETAIL'));
    return userData.email;
  }


  login(email, password) {
    const body = {
      email: email,
      password: password
    }

    return this.http.post<any>(`${GlobalConstants.API_URL}/users/login`, body).pipe(tap(res => {
      console.log(res);
      this.loggedIn.next(true);
      localStorage.setItem('ACCESS_TOKEN', res.token);
      localStorage.setItem('USER_DETAIL', `{"email":"${res.email}", "id":${res.id}}`)
    }),
      catchError(this.handleError))
  }

  register(firstName, lastName, email, password) {
    const body = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password
    }

    return this.http.post<any>(`${GlobalConstants.API_URL}/users/register`, body).pipe(tap(res => console.log(res)),
      catchError(this.handleError))
  }

  get isLoggedIn(){
    return this.loggedIn.asObservable();
  }

  isAuthenticated() {
    const token = localStorage.getItem('ACCESS_TOKEN');
    if (token) {
      return true;
    }
    return false;
  }

  signOut() {
    this.loggedIn.next(false);
    localStorage.removeItem('ACCESS_TOKEN');
    localStorage.removeItem('USER_DETAIL');
  }

  handleError(error) {

    if (error.error.name === "InvalidCredentials" || error.error.name === "NoUserFound") {
      return throwError(error.error.message || 'Server Error!');
    }
    else {
      console.log(error);
      return throwError('Something Went wrong Please try again later!')
    }
  }
}
