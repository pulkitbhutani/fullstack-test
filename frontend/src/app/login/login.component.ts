import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AccountService } from '../services/account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  submitted = false;
  loginError: string = '';

  loginForm = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required]
  })

  constructor(private formBuilder: FormBuilder, private accountService: AccountService, private router: Router) { }

  ngOnInit(): void {
  }

  get loginFormItems() { return this.loginForm.controls; }

  onSubmit() {
    this.loginError= '';
    this.submitted = true;
    if (this.loginForm.invalid) {
      console.log('form invalid!');
      return;
    }

    this.accountService.login(this.loginForm.value.email, this.loginForm.value.password).subscribe((data)=>{
      console.log('ok',data);
      this.router.navigate(['/']);
    },(error) => {
      console.log('err', error);
      this.loginError = error;
    });
  }

}
