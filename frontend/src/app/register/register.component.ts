import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountService } from '../services/account.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  submitted = false;
  registerError = '';
  registerSuccess = false;

  registerForm = this.formBuilder.group({
    firstName: ['', Validators.required],
    lastName: [''],
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(6)]]
  })

  constructor(private formBuilder: FormBuilder, private accountService: AccountService, private router: Router) { }

  ngOnInit(): void {
  }

  get registerFormItems() { return this.registerForm.controls }

  onSubmit() {

    this.submitted = true;

    if (this.registerForm.invalid) {
      console.log('Form entries invalid!');
      return;
    }

    this.accountService.register(this.registerForm.value.firstName, this.registerForm.value.lastName, this.registerForm.value.email, this.registerForm.value.password)
      .subscribe((data) => {
        console.log('ok', data);
        this.registerSuccess = true;
      }, (error) => {
        console.log(error);
        this.registerError = error;
      });

    console.log(this.registerForm.value);

    this.submitted = true;
  }

}
