import { Component, OnInit } from '@angular/core';
import { AccountService } from './services/account.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'frontend';
  isAuthenticated = false;
  isLoggedIn: Observable<boolean>;
  constructor(private accountSevice: AccountService) {
  }

  ngOnInit() {
    this.isLoggedIn = this.accountSevice.isLoggedIn;
    this.isAuthenticated = this.accountSevice.isAuthenticated();
  }

}
