import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent implements OnInit {

  departmentData: any;

  constructor(public router: Router) { }

  ngOnInit(): void {
  }

  goToAddDept(){
    this.router.navigate(['/departments/add-department']);
  }

}
