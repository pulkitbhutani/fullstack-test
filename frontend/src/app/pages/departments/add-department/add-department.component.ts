import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DepartmentsService } from '../departments.service';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-add-department',
  templateUrl: './add-department.component.html',
  styleUrls: ['./add-department.component.css']
})
export class AddDepartmentComponent implements OnInit {


  addDeptSuccess = false;
  addDeptError = false;
  errorMessage = '';
  addDeptForm = this.formBuilder.group({
    orgName: [''],
    deptOwner: [''],
    description: [''],
    openTime: [''],
    closeTime: [''],
    workingDays: ['']
  })
  organizationsData;

  constructor(public formBuilder: FormBuilder, public departmentService: DepartmentsService) { }

  ngOnInit(): void {
   this.departmentService.getAllOrganizationsNames().subscribe((data)=>{
     console.log(data);
     this.organizationsData = data;
   })
  }

  onSubmit() {

  }

}
