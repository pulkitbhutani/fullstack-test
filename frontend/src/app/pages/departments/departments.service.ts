import { Injectable } from '@angular/core';
import { GlobalConstants } from '../../utility/constants';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DepartmentsService {

  constructor(private httpClient: HttpClient) { }

  //Get Organizations names list for Add Depratments page
  getAllOrganizationsNames() {
    return this.httpClient.get<any>(`${GlobalConstants.API_URL}/organizations`).pipe(map(orgs => orgs.map((org)=> ({id: org.id, name: org.orgName}))));
  }
}
