import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { OrganizationService } from '../organization.service';
@Component({
  selector: 'app-add-organization',
  templateUrl: './add-organization.component.html',
  styleUrls: ['./add-organization.component.css']
})
export class AddOrganizationComponent implements OnInit {

  addOrgSuccess = false;
  addOrgError = false;
  errorMessage = '';
  addOrgForm = this.formBuilder.group({
    orgName: [''],
    ownerName: [''],
    address: [''],
    city: [''],
    state: [''],
    country: ['']
  })

  constructor(public formBuilder: FormBuilder, public organizationService: OrganizationService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    console.log(this.addOrgForm);
    this.errorMessage = '';
    this.addOrgError = false;
    this.organizationService.addOrganization(this.addOrgForm.value).subscribe((res) => {
      console.log(res);
      this.addOrgSuccess = true;
    }, (error) => {
      console.log(error);
      this.addOrgError = true;
      this.errorMessage = error;
    });
  }

}
