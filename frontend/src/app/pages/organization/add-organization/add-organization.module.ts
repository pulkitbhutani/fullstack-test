import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AddOrganizationRoutingModule } from './add-organization-routing.module';
import { AddOrganizationComponent } from './add-organization.component';


@NgModule({
  declarations: [AddOrganizationComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AddOrganizationRoutingModule
  ]
})
export class AddOrganizationModule { }
