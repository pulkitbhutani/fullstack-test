import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import { EditOrganizationRoutingModule } from './edit-organization-routing.module';
import { EditOrganizationComponent } from './edit-organization.component';


@NgModule({
  declarations: [EditOrganizationComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    EditOrganizationRoutingModule
  ]
})
export class EditOrganizationModule { }
