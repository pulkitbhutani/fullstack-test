import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { OrganizationService } from '../organization.service';
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { map, switchMap } from 'rxjs/operators';
@Component({
  selector: 'app-edit-organization',
  templateUrl: './edit-organization.component.html',
  styleUrls: ['./edit-organization.component.css']
})
export class EditOrganizationComponent implements OnInit {

  orgId: Number;
  editOrgSuccess = false;
  editOrgError = false;
  orgData;
  errorMessage = '';

  editOrgForm = this.formBuilder.group({
    orgName: [''],
    ownerName: [''],
    address: [''],
    city: [''],
    state: [''],
    country: ['']
  })


  constructor(public formBuilder: FormBuilder, public organizationService: OrganizationService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.orgId = +params.get('id');
      this.organizationService.getOrganizationDetails(this.orgId).subscribe((data) => {
        this.orgData = data;
        this.editOrgForm.setValue({
          orgName: this.orgData.orgName,
          ownerName: this.orgData.ownerName,
          address: this.orgData.address,
          city: this.orgData.city,
          state: this.orgData.state,
          country: this.orgData.country
        });
      });
    })
    //this.organizationService.getOrganizationDetails()
  }

  onSubmit() {
    console.log(this.editOrgForm);
    this.errorMessage = '';
    this.editOrgError = false;
    this.organizationService.updateOrganization(this.orgId, this.editOrgForm.value).subscribe((res) => {
      console.log(res);
      this.editOrgSuccess = true;
    }, (error) => {
      console.log(error);
      this.editOrgError = true;
      this.errorMessage = error;
    });
  }

}
