import { Component, OnInit } from '@angular/core';
import { OrganizationService } from './organization.service';
@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.css']
})
export class OrganizationComponent implements OnInit {

  organizationData: any;

  constructor(private orgService: OrganizationService) { }

  ngOnInit(): void {
    this.orgService.getAllOrganizations().subscribe((data) => {
      this.organizationData = data;
      //console.log(data);
    })
  }


}
