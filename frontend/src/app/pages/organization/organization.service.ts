import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AccountService } from '../../services/account.service';
import { GlobalConstants } from '../../utility/constants';
import { tap, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  loggedInUserId = this.accountService.getUserId();

  constructor(private accountService: AccountService, private httpClient: HttpClient) { }

  //Get all organization data for organizations page
  getAllOrganizations() {
    return this.httpClient.get(`${GlobalConstants.API_URL}/organizations`);
  }

  getOrganizationDetails(id) {
    return this.httpClient.get(`${GlobalConstants.API_URL}/organizations/${id}`);
  }

  addOrganization(orgData) {
    const body = { userId: this.loggedInUserId, ...orgData }
    console.log('body', body);
    return this.httpClient.post<any>(`${GlobalConstants.API_URL}/organizations/add-organization`, body).pipe(tap(res => console.log(res)), catchError(this.handleError));
  }

  updateOrganization(orgId, orgData) {
    const body = { id: orgId, userId: this.loggedInUserId, ...orgData }
    console.log('body', body);
    return this.httpClient.put<any>(`${GlobalConstants.API_URL}/organizations/update-organization`, body).pipe(tap(res => console.log(res)), catchError(this.handleError));
  }

  handleError(error) {
    if (error.error.name === "InvalidCredentials" || error.error.name === "NoUserFound") {
      return throwError(error.error.message || 'Server Error!');
    }
    else {
      console.log(error);
      return throwError('Something Went wrong Please try again later!')
    }
  }
}
