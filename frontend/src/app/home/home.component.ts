import { Component, OnInit } from '@angular/core';
import { AccountService } from '../services/account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


constructor(private accountService: AccountService, public router: Router) { }

ngOnInit(): void {
}



getUserData(){
  this.accountService.getUserId();
}


signOut() {
  this.accountService.signOut();
  this.router.navigate(['login']);
}

}
