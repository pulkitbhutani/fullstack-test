import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './guards/auth.guard';
import { GoogleChartsModule } from 'angular-google-charts';

const routes: Routes = [
  {
    path: '', component: HomeComponent, canActivate: [AuthGuard]
  },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  {
    path: 'organization',
    children: [
      { path: '', loadChildren: () => import('./pages/organization/organization.module').then(m => m.OrganizationModule), canActivate: [AuthGuard] },
      { path: 'addOrganization', loadChildren: () => import('./pages/organization/add-organization/add-organization.module').then(m => m.AddOrganizationModule), canActivate: [AuthGuard] },
      { path: 'editOrganization/:id', loadChildren: () => import('./pages/organization/edit-organization/edit-organization.module').then(m => m.EditOrganizationModule), canActivate: [AuthGuard] },
    ]
  },
  {
    path: 'departments',
    children: [
      { path: '', loadChildren: () => import('./pages/departments/departments.module').then(m => m.DepartmentsModule), canActivate: [AuthGuard] },
      { path: 'add-department', loadChildren: () => import('./pages/departments/add-department/add-department.module').then(m => m.AddDepartmentModule), canActivate: [AuthGuard] }]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), GoogleChartsModule.forRoot({ version: 'chart-version' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
